[console_scripts]
repomaker-server = repomaker:runserver
repomaker-tasks = repomaker:process_tasks

[gui_scripts]
repomaker = repomaker.gui:main

